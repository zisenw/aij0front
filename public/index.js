// 环境兼容
const productEnv = 'aij0.com';
const testEnv = 'test.aij0.com';
const productApi = 'http://152.32.191.6:8888';
const testApi = 'http://152.32.191.6:8889';
let current;
let redirect; // 跳转url
if (window.location.hostname === productEnv){
    current = productApi;
    redirect = `http://${productEnv}`;
}else{
    current = testApi;
    redirect = `http://${testEnv}`;
}

// api
const submitApi = `${current}/douyinTemplate/create`;  // 模板提交api
// const imgUploadApi = `${current}/api/document/uploadImage`;  // 图片上传api
const imgUploadApi = `${current}/douyinTemplate/uploadTemplateImage`;  // 图片上传api
const getApi = `${current}/douyinTemplate/get`;  // 模板获取api
const editApi = `${current}/douyinTemplate/edit`; // 模板修改api
const deleteApi =`${current}/douyinTemplate/delete`; // 模板删除api
const userApi = `${current}/api/user/detail`; // user
// const submitApi = 'http://localhost:9000/douyinTemplate/create';  // 模板提交api
// const imgUploadApi = 'http://localhost:9000/api/document/uploadImage';  // 图片上传api
// const getApi = 'http://localhost:9000/douyinTemplate/get';  // 模板获取api
// const editApi = 'http://localhost:9000/douyinTemplate/edit'; // 模板修改api
// const deleteApi ='http://localhost:9000/douyinTemplate/delete'; // 模板删除api


// 图片消失
const imgDisappear = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wrJ+XwAAAABJRU5ErkJggg==';
// 一页加载的模板数量
const number = 20;
// 模拟的模板数据
let templates = {};

// token
// let token = 'uKoxfmBL9VTQ7fooYdtl3vVvg0dsMrxd';
let token = localStorage.getItem("token");

// 图像上传缓存
let imgUploadUrls ={};

// 表单提交数据模型
let templateCreate = {};

// 获取表格的 tbody 元素
let tbody = document.querySelector('#templateTable tbody');

// 全局判断模板保存的入口,0为create，1为edit
let createOrEdit = 0;

// 上传逻辑变量
const background = document.querySelector('#background');
const foreground = document.querySelector('#foreground');
const thumbnail = document.querySelector('#thumbnail');
const backbtn = document.querySelector('#backgroundBtn');
const forebtn = document.querySelector('#foregroundBtn');
const thumbbtn = document.querySelector('#thumbnailBtn');
const backImg = document.querySelector('#backImg');
const foreImg = document.querySelector('#foreImg');
const thumbImg = document.querySelector('#thumbImg');

// 上传按钮绑定事件
backbtn.addEventListener('click', ()=>clickUpload(background));
forebtn.addEventListener('click', ()=>clickUpload(foreground));
thumbbtn.addEventListener('click', ()=>clickUpload(thumbnail));

background.addEventListener('change', (event) => uploadImg(event,backImg));
foreground.addEventListener('change', (event) => uploadImg(event,foreImg));
thumbnail.addEventListener('change', (event) => uploadImg(event,thumbImg));


const slotSelect = document.getElementById('slot');
const parameterContainer = document.getElementById('parameterContainer');

// 根据slot确定图片信息填写数
slotSelect.addEventListener('change', function() {
    let selectedValue = parseInt(slotSelect.value); // 获取选择的次数
    renderParameterInputs(selectedValue,null); // 根据选择的次数生成参数输入位置
});


// 提交绑定事件
document.querySelector("#submit").addEventListener('click',customSubmit);

/**
 * 渲染表格
 */
function renderTable(page){
    tbody.innerHTML='';
    // 动态生成表格内容
    templates[page].forEach(function(template) {
        let row = document.createElement('tr');
        let info = JSON.parse(template.info);
        console.log(info);
        // 创建 ID 列
        let idCell = document.createElement('td');
        idCell.textContent = template.id;
        row.appendChild(idCell);

        // 创建名称列
        let nameCell = document.createElement('td');
        nameCell.textContent = info.templateName || '' ;
        row.appendChild(nameCell);

        // 创建slot列
        let paramCell = document.createElement('td');
        paramCell.textContent = template.slot;
        row.appendChild(paramCell);

        // 创建类目列
        let categoryCell = document.createElement('td');
        categoryCell.textContent = categorySort(info.category);
        row.appendChild(categoryCell);

        // 创建操作列
        let operationCell = document.createElement('td');
        let edit = document.createElement('a');
        let del = document.createElement('a');
        // 创建操作按钮的容器
        let actionButtons = document.createElement('div');
        actionButtons.className = 'action-buttons';
        edit.href = "javascript:void(0)";
        edit.textContent = '编辑';
        edit.onclick = () => handleEdit(template.id,info,template);
        del.href = "javascript:void(0)";
        del.textContent = '删除';
        del.onclick = () => handleDelete(template.id);
        actionButtons.appendChild(edit);
        actionButtons.appendChild(del);
        row.appendChild(operationCell).appendChild(actionButtons);
        // 将行添加到表格的 tbody 中
        tbody.appendChild(row);
    });
}
/**
 * 弹窗渲染位置信息输入框
 * @param count
 * @param info
 */
function renderParameterInputs(count,info) {
    let flag = info !== null;
    // 清空参数输入位置的内容
    parameterContainer.innerHTML = '';

    // 根据选择的次数生成参数输入位置
    for (let i = 0; i < count; i++) {
        let parent = document.createElement('tr');
        let paramParent = document.createElement('td');
        let label = document.createElement('label');
        label.textContent = '位置' + (i + 1);

        // x坐标
        let x = document.createElement('input');
        x.type = 'number';
        x.className = i.toString();
        x.name = "x";
        x.placeholder = "x"
        x.style.width='40px';
        x.style.marginRight='20px';
        if (flag) x.value = info.pos[i+1].x;

        // y坐标
        let y = document.createElement('input');
        y.type = 'number';
        y.className = i.toString();
        y.name = "y";
        y.placeholder = "y";
        y.style.width='40px';
        y.style.marginRight='20px';
        if (flag) y.value = info.pos[i+1].y;

        // 宽度
        let width = document.createElement('input');
        width.type = 'number';
        width.className = i.toString();
        width.name = "width";
        width.placeholder = "width"
        width.style.width='40px';
        width.style.marginRight='20px';
        if (flag) width.value = info.pos[i+1].width;


        // 高度
        let height = document.createElement('input');
        height.type = 'number';
        height.className = i.toString();
        height.name = "height";
        height.placeholder = "height"
        height.style.width='40px';
        height.style.marginRight='20px';
        if (flag) height.value = info.pos[i+1].height;


        // seg抠图
        let seg = document.createElement('select');
        seg.className = i.toString();
        seg.name = "seg";
        seg.style.marginRight = '20px';
        let seg0 = document.createElement('option');
        seg0.value = '0';
        seg0.textContent = '抠图';
        let seg1 = document.createElement('option');
        seg1.value = '1';
        seg1.textContent = '生成背景';
        if (flag){
            info.pos[i+1].seg == '1'? seg1.selected = true : seg0.selected = true;
        }else{
            seg0.selected = true;
        }
        seg.appendChild(seg0);
        seg.appendChild(seg1);


        // isClip
        let clip = document.createElement('select');
        clip.className = i.toString();
        clip.name = "isClip";
        let op0 = document.createElement('option');
        op0.value = '0';
        op0.textContent = '不固定范围显示';
        let op1 = document.createElement('option');
        op1.value = '1';
        op1.textContent = '固定范围显示';
        if (flag){
            info.pos[i+1].isClip == '1'? op1.selected = true : op0.selected = true;
        }else{
            op1.selected = true;
        }
        clip.appendChild(op0);
        clip.appendChild(op1);

        // 添加元素进dom
        parent.appendChild(document.createElement('td')).appendChild(label);
        paramParent.appendChild(x);
        paramParent.appendChild(y);
        paramParent.appendChild(width);
        paramParent.appendChild(height);
        paramParent.appendChild(seg);
        paramParent.appendChild(clip);
        parameterContainer.appendChild(parent).appendChild(paramParent);
    }
}
// /**
//  * 弹窗中获取更新id
//  */
// function getUpdateId(){
//     return templates[templates.length-1].id+1
// }
/**
 * 处理上传点击按钮
 * @param pos 上传对应input
 */
function clickUpload(pos){
    pos.value = null;
    pos.click();
}

/**
 * 上传input点击后进行上传
 * @param event
 * @param img
 */
async function uploadImg(event,img) {
    event.stopPropagation();
    if (event.target.files.length <= 0){
        return;
    }
    //上传至服务器
    let file = event.target.files[0];
    const form = new FormData();
    form.append('image', file);
    uploadAjaxApi(form, img, renderImg);

}
/**
 * 打开弹窗，新增模板入口
 */
function openPopup() {
    templateCreate={};
    imgUploadUrls={};
    backImg.src = imgDisappear;
    foreImg.src = imgDisappear;
    thumbImg.src = imgDisappear;
    slotSelect.value = 2;
    document.getElementById('category').value = 'women';
    // 根据回传内容填写更新id
    // document.querySelector("#idCell").textContent=getUpdateId();
    let popup = document.getElementById('popup');
    document.getElementById('popup-title').textContent = "新增模板"
    let popupid = document.getElementsByClassName('popup-id');
    console.log(popupid);
    for (let temp of popupid){
        temp.style.display="none";
    }
    createOrEdit = 0;
    popup.style.display = 'block';
    renderParameterInputs(slotSelect.value,null);
}

/**
 * 关闭弹窗
 */
function closePopup() {
    let popup = document.getElementById('popup');
    popup.style.display = 'none';
}

/**
 * ajax上传，成功后调用渲染函数
 */
function uploadAjaxApi(form, target, callback) {
    //原生Ajax
    const xhr = new XMLHttpRequest();
    let res;
    xhr.open('POST', imgUploadApi);
    xhr.setRequestHeader("token",token);
    xhr.onload = function () {
        if (xhr.status === 200 && JSON.parse(xhr.response).code == 0){
            res = JSON.parse(xhr.responseText);
            console.log(res);
            callback(res.data.data.url[0], target);
        }else if(JSON.parse(xhr.response).code == 401){
            // 请求失败，处理错误
            // token = null;
            window.location.href = redirect;
        }else{
            console.error('Request failed. Status:', xhr.status);
        }

    }
    xhr.send(form)
    return res;
}

/**
 * 缓存图片信息，并渲染至页面
 * @param url
 * @param element
 */
function renderImg(url, element){
    // 缓存
    imgUploadUrls[element.id] = url;
    // 渲染
    element.src = url;
    // element.style.maxWidth = '100px';
    // element.style.maxHeight = '100px';
}

/**
 * 自定义提交表单
 */
function customSubmit(){
    // 处理数据
    templateCreate.templateName = document.getElementById('templateName').value;
    templateCreate.slot = parseInt(document.getElementById('slot').value);
    templateCreate.background = imgUploadUrls.backImg?.toString() ?? null;
    templateCreate.foreground = imgUploadUrls.foreImg?.toString() ?? null;
    templateCreate.url = imgUploadUrls.thumbImg?.toString() ?? '';
    templateCreate.category = document.getElementById('category').value;
    templateCreate.pos = {};
    // 处理位置信息
    for (let i=0 ; i<templateCreate.slot ; i++){
        let tempCol = document.getElementsByClassName(i.toString());
        let tempRes = {}
        console.log(tempCol);
        for (let temp in tempCol){
            tempRes[tempCol[temp].name] = parseInt(tempCol[temp].value);
        }
        //TODO: 未来填图底版参数
        tempRes.img=null;
        tempRes.layer = i+1;
        templateCreate.pos[(i+1).toString()] = tempRes;
    }
    // 提交表单
    templateCreate.pos = JSON.stringify(templateCreate.pos);
    const xhr = new XMLHttpRequest();
    if(createOrEdit == 0){
        xhr.open('POST', submitApi);
    }else{
        templateCreate.id = parseInt(document.getElementById('idCell').textContent);
        xhr.open("POST",editApi);
    }
    xhr.onload = function () {
        if (xhr.status === 200 && JSON.parse(xhr.response).code == 0) {
            console.log(xhr.responseText);
            closePopup();
            templates = {};
            getData(0,true,renderPagination);
            showSuccessMessage("保存成功");
        }else if(JSON.parse(xhr.response).code == 401){
            // 请求失败，处理错误
            // token = null;
            window.location.href = redirect;
        }else{
            console.error('Request failed. Status:', xhr.status);
        }
    }
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(templateCreate));
}

/**
 * 获取数据
 * @param page
 * @param refresh decide whether using memory or not
 * @param callback
 */
function getData(page, refresh, callback){
    if (templates.hasOwnProperty(page) && refresh === false){
        renderTable(page);
        return;
    }
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${getApi}?page=${page}&number=${number}`);
    xhr.setRequestHeader("token",token);
    xhr.onload = function() {
        if (xhr.status === 200 && JSON.parse(xhr.response).code == 0) {

            // 请求成功，处理响应数据
            let response = JSON.parse(xhr.response);
            console.log(response);
            templates[page] = response.data.data.res;
            let total = response.data.data.total;
            console.log(templates);
            renderTable(page);
            if (callback != null) callback(total);
        } else if(JSON.parse(xhr.response).code == 401){
            // 请求失败，处理错误
            // token = null;
            window.location.href = redirect;
        } else {
            // 请求失败，处理错误
            console.error('Request failed. Status:', xhr.status);
        }
    };

    xhr.send();
}

/**
 * 类目显示字段返回
 * @param cat
 * @returns {string}
 */
function categorySort(cat){
    switch (cat){
        case "women":
            return "女装";
        case "men":
            return "男装";
        default:
            break;
    }
}
const successMessage = document.getElementById('successMessage');

// 显示上传成功提示框
function showSuccessMessage(text) {
    // 显示提示框
    successMessage.textContent = text;
    successMessage.style.display = 'block';
    setTimeout(function() {
        hideSuccessMessage();
    }, 2000);
}

// 隐藏上传成功提示框
function hideSuccessMessage() {
    // 隐藏提示框
    successMessage.style.display = 'none';
}

/**
 * 渲染分页
 * @param num
 */
function renderPagination(num){
    const slp = new SimplePagination(num)
    slp.init({
        container: '#pagination',
        maxShowBtnCount: 3,
        onPageChange: state => {
            console.log('pagination change:', state.pageNumber);
            getData(state.pageNumber-1,false,null);
        }
    })
}

/**
 * 点击编辑
 * @param id
 * @param info
 * @param template
 */
function handleEdit(id, info, template){
    templateCreate={};
    imgUploadUrls={};
    // 全局入口
    createOrEdit = 1;
    //预渲染
    let popup = document.getElementById('popup');
    document.getElementById('popup-title').textContent = "编辑模板"
    for (let temp of document.getElementsByClassName('popup-id')){
        temp.style.display='';
    }
    let idCell = document.getElementById('idCell');
    idCell.textContent = id;
    //模板名
    let templateName = document.getElementById('templateName');
    templateName.value = info.templateName || '';
    //拼图数
    let slot = document.getElementById('slot');
    slot.value = parseInt(template.slot);
    //类目
    let category = document.getElementById('category');
    category.value = info.category || '';

    // 图片
    let backImg = document.getElementById('backImg');
    if (info.background?.length ?? 0 >0){
        backImg.src =info.background;
        imgUploadUrls.backImg = backImg.src;
    }else{
        backImg.src = imgDisappear;
    }


    let foreImg = document.getElementById('foreImg');
    if (info.foreground?.length ?? 0 >0) {
        foreImg.src = info.foreground;
        imgUploadUrls.foreImg = foreImg.src;
    }else{
        foreImg.src = imgDisappear;
    }

    let thumbImg = document.getElementById('thumbImg');
    if (template.url?.length ?? 0 > 0){
        thumbImg.src =template.url;
        imgUploadUrls.thumbImg = thumbImg.src;
    }else{
        thumbImg.src = imgDisappear;
    }


    renderParameterInputs(slot.value,info);
    //打开弹窗
    popup.style.display = 'block';

}

function handleDelete(id){
    const xhr = new XMLHttpRequest();
    xhr.open('POST', deleteApi);
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        if (xhr.status === 200 && JSON.parse(xhr.response).code == 0) {
            // 请求成功，处理响应数据
            showSuccessMessage("删除成功");
            templates={};
            getData(0,true,renderPagination);
        } else if(JSON.parse(xhr.response).code == 401){
            // 请求失败，处理错误
            // token = null;
            window.location.href = redirect;
        } else {
            // 请求失败，处理错误
            console.error('Request failed. Status:', xhr.status);
        }
    };
    let ob = {};
    ob.id = parseInt(id);
    xhr.send(JSON.stringify(ob));
}

function getToken(callback){
    const xhr = new XMLHttpRequest();
    xhr.open('GET', userApi);
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        if (xhr.status === 200 && JSON.parse(xhr.responseText).code == 0) {
            // 请求成功，处理响应数据
            //     token = localStorage.getItem("token");
            callback();
        } else {
            // 请求失败，处理错误
            // token = null;
            window.location.href = redirect;
        }
    };
    xhr.send();
}

window.onload = function(){
    hideSuccessMessage();
    getToken(() => getData(0,true,renderPagination));
}